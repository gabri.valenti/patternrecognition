// Constants

const fs = require("fs");
const express = require('express');
const bodyParser = require('body-parser');

const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')


const PORT = 30001;
const app = express();

const adapter = new FileSync('db.json')
const db = low(adapter)

///////////////////////////////////////////////////////////

// Set some defaults (required if your JSON file is empty)
db.defaults({ space: [], count: 0 }).write()

app.use(bodyParser.json());

///////////////////////////////////////////////////////////
//Web API

app.get('/space', (req, res) => {
   
   try {
		//Recupero l'intero spazio di punti
		const space = db.get('space').value();

		var count = GetCount(space);
   
		res.send(JSON.stringify({"status": 200, "message": "point in the space: " + count, "response": db.get('space')}));
	}
	catch(err) {
		res.send(JSON.stringify({"status": 500, "message": err, "response": 'Error to get space point'}));
	}
});

app.post('/point', (req, res) => {

	try {

		if(!isNumber(req.body.X) || !isNumber(req.body.Y)){
			res.send(JSON.stringify({"status": 200, "message": "Body values are not a number", "response": null  }));
		}
		else{
			console.log('X: ' + req.body.X + ', Y: '+ req.body.Y);
	
			const el = db.get('space').find({
				"X": req.body.X,
				"Y": req.body.Y
			}).value();	

			//Controllo se esiste già il punto passato in input
			if( typeof el !== 'undefined' ) {
				console.log('Data: (' + el.X + ',' +  el.Y + ')');

				res.send(JSON.stringify({"status": 200, "message": null, "response": 'Point already exist'}));
			}
			//Se non esiste inserisco il nuovo punto
			else{
				
				db.get('space').push({ X: req.body.X, Y: req.body.Y}).write();

				var mex = 'Add new point: (X: ' +  req.body.X + ',Y: ' + req.body.Y + ')';
				console.log(mex);

				res.send(JSON.stringify({"status": 200, "message": null, "response": mex}));
			}	
		}		
	}
	catch(err) {
		res.send(JSON.stringify({"status": 500, "message": err, "response": 'Error to add new point'}));
	}	
});

app.delete('/space', (req, res) => {
	try {
		//Cancello l'intero spazio dei punti
		db.set('space', []).write();

		res.send(JSON.stringify({"status": 200, "message": null, "response": 'Delete space correctly'}));
	}
	catch(err) {
		res.send(JSON.stringify({"status": 500, "message": err, "response": 'Error to delete space'}));
	}	
});


app.get('/lines/:n', (req, res) => {
   
	try {
		var n = req.params.n;
		console.log('N: ' + n);

		if(!isNumber(n)){
			res.send(JSON.stringify({"status": 200, "message": "Input n is not a number", "response": null  }));
		}
		else if(n < 0){
			res.send(JSON.stringify({"status": 200, "message": "Input n is not greater than zero", "response": null  }));
		}
		else{
			var points = db.get('space').value();

			if(points !== 'undefined' && points.length >= n){

				//Recupero tutte le possibili linee
				var result = combine(points, n);

				var count = GetCount(result);

				res.send(JSON.stringify({"status": 200, "message": "Lines: " +  count, "response": result  }));
			}
			else res.send(JSON.stringify({"status": 200, "message": "Lines: 0", "response": null  }));
			
		}		
	 }
	 catch(err) {
		 res.send(JSON.stringify({"status": 500, "message": err, "response": 'Error to get space point'}));
	 }
 });


///////////////////////////////////////////////////////////
//Function

//Ritorna il numero di elementi dell'array
function GetCount(array){
	var count = 0;

	if( typeof array !== 'undefined' ) count = array.length;

	console.log('Count: ' +  count);

	return count;
}

 //Ritorna true se il valore in input è un numero
function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}


//Ritorna le size combinazioni possibili degli elementi dell'array
function combine(array, size) {

	function c(part, start) {
		var result = [], i, l, p;
		for (i = start, l = array.length; i < l; i++) {
			p = part.slice(0);                       // get a copy of part
			p.push(array[i]);                        // add the iterated element to p

			if (p.length < size) {                   // test if recursion can go on
				result = result.concat(c(p, i + 1)); // call c again & concat rresult
			} else {
				result.push(p);                      // push p to result, stop recursion
			}
		}
		return result;
	}

	return c([], 0);
}


///////////////////////////////////////////////////////////
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});