﻿using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PatternRecognitionResources.PatternDto
{
    [DataContract]
    public class PointDto
    {
        [DataMember(Name = "x")]
        [JsonProperty("x")]
        [Required]
        public double X { get; set; }

        
        [DataMember(Name = "y")]
        [JsonProperty("y")]
        [Required]
        public double Y { get; set; }
    }
}
