﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PatternRecognitionResources.PatternDto
{
    [DataContract]
    public class LineDto
    {
        [DataMember(Name = "line")]
        [JsonProperty("line")]
        public List<PointDto> Points { get; set; }
    }
}
