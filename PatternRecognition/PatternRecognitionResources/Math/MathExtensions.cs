﻿using System.Collections.Generic;
using System.Linq;

namespace PatternRecognitionResources.Math
{
    public static class MathExtensions
    {
        /// <summary>
        /// Ritorna tutte le possibili combinazioni
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<T>> Combinations<T>(this IEnumerable<T> source, int n)
        {
            if (n == 0) yield return Enumerable.Empty<T>();

            int count = 1;

            var enumerable = source as T[] ?? source.ToArray();

            foreach (T item in enumerable)
            {
                foreach (var innerSequence in enumerable.Skip(count).Combinations(n - 1))
                {
                    yield return new T[] { item }.Concat(innerSequence);
                }

                count++;
            }
        }
    }
    
}
