﻿using System.Runtime.Serialization;
using System.Diagnostics;
using Newtonsoft.Json;
using System;

namespace PatternRecognitionResources.GenericDto
{
    /// <summary>
    /// Gestione Result
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract]
    public class ResultDto<T>
    {
        /***********************************************/
        //Campi

        [JsonProperty("Data")]
        [DataMember(Name = "Data")]
        public T Data { get; set; }

        [JsonProperty("Status")]
        [DataMember(Name = "Status")]
        public bool Status { get; set; }

        [JsonProperty("FunctionName")]
        [DataMember(Name = "FunctionName")]
        public string FunctionName { get; set; }

        [JsonProperty("Message")]
        [DataMember(Name = "Message")]
        public string Message { get; set; }
        

        /***********************************************/
        //Costruttori

        public ResultDto()
        {

        }

        public ResultDto(bool status)
        {
            Status = status;
            FunctionName = new StackTrace().GetFrame(1).GetMethod().Name;
        }

        public ResultDto(bool status, string message)
        {
            Status = status;
            Message = message;
            FunctionName = new StackTrace().GetFrame(1).GetMethod().Name;
        }

        public ResultDto(T data, bool status)
        {
            Data = data;
            Status = status;
            FunctionName = new StackTrace().GetFrame(1).GetMethod().Name;
        }

        public ResultDto(T data, bool status, string message)
        {
            Data = data;
            Status = status;
            Message = message;
            FunctionName = new StackTrace().GetFrame(1).GetMethod().Name;
        }

        public ResultDto(Exception ex)
        {
            Status = false;
            Message = GetEx(ex);
            FunctionName = new StackTrace().GetFrame(1).GetMethod().Name;
        }

        public ResultDto(Exception ex, T data)
        {
            Data = data;
            Status = false;
            Message = GetEx(ex);
            FunctionName = new StackTrace().GetFrame(1).GetMethod().Name;
        }

        public string PrintResult()
        {
            return $"Status: {Status}" +
                   (!string.IsNullOrEmpty(Message) ? $", message: {Message}" : string.Empty) +
                   (!string.IsNullOrEmpty(FunctionName) ? $", function: {FunctionName}" : string.Empty);
        }

        /*******************************************************************/

        private string GetEx(Exception ex)
        {
            return $"Error: {ex.Message}, stack trace: {ex.StackTrace}";
        }
    }
}
