﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace DAL.Dto
{
    [DataContract]
    public class PointResultDto
    {
        [DataMember(Name = "id")]
        [JsonProperty("id")]
        public int Id { get; set; }

        [DataMember(Name = "x")]
        [JsonProperty("x")]
        public double X { get; set; }


        [DataMember(Name = "y")]
        [JsonProperty("y")]
        public double Y { get; set; }
    }
}
