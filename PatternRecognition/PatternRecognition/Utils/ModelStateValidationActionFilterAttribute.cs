﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;

namespace PatternRecognition.Utils
{
    public class ModelStateValidationActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var modelState = actionContext.ModelState;

            if (!modelState.IsValid)
            {
                //Creo la risposta custom se il model state non è valido
                var errorStatus = GenericHelper.GetModelNotValidStatusCode(modelState);
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, errorStatus);
            }
        }
    }
}