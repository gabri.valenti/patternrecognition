﻿using System;
using System.Net;
using System.Net.Http;
using PatternRecognition.Dto;
using FluentValidation.Results;
using System.Collections.Generic;
using System.Web.Http.ModelBinding;


namespace PatternRecognition.Utils
{
    public static class GenericHelper
    {
        /// <summary>
        /// Gestisce la risposta dovuta ad eccezione 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ex"></param>
        /// <returns></returns>
        internal static HttpResponseMessage CreateResponse(HttpRequestMessage request, Exception ex)
        {
            return request.CreateResponse(HttpStatusCode.InternalServerError, new GenericErrors { Message = ex.Message, ExtendedInfo = ex.StackTrace });
        }

        /*******************************************************************/
        /*******************************************************************/

        #region ValidationFields

        /// <summary>
        /// Crea una risposta se i dati in input non sono corretti
        /// </summary>
        /// <param name="result"></param>
        /// <param name="extendedInfo"></param>
        /// <returns></returns>
        internal static GenericErrors CreateErrorValidationResponse(ValidationResult result, string extendedInfo = null)
        {
            GenericErrors response = new GenericErrors();

            response.Message = "Input body data is not correct";
            response.ExtendedInfo = extendedInfo;

            //Se ho degli errori li mappo
            if (result.Errors.Count > 0)
            {
                var validationErrors = new List<ValidationErrorsType>();

                foreach (var error in result.Errors)
                {
                    var validationError = new ValidationErrorsType();

                    validationError.ErrorMessage = error.ErrorMessage;
                    validationError.FieldReference = error.PropertyName;

                    validationErrors.Add(validationError);
                }

                response.ValidationError = validationErrors.ToArray();
            }

            return response;
        }

        /// <summary>
        /// Recupera gli errori dal model state
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        internal static List<string> GetModelStateError(ModelStateDictionary modelState)
        {
            List<string> modelErrors = new List<string>();

            foreach (var model in modelState.Values)
            {
                foreach (var modelError in model.Errors)
                {
                    modelErrors.Add(modelError.ErrorMessage);
                }
            }

            return modelErrors;
        }

        /// <summary>
        /// Ritorna uno status Bad Request
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        internal static GenericErrors GetModelNotValidStatusCode(ModelStateDictionary modelState)
        {
            GenericErrors response = new GenericErrors();

            response.Message = "Input data are not correct";

            try
            {
                var validationErrors = new List<ValidationErrorsType>();

                foreach (var model in modelState.Values)
                {
                    foreach (var modelError in model.Errors)
                    {
                        validationErrors.Add(new ValidationErrorsType { ErrorMessage = modelError.ErrorMessage });
                    }
                }

                response.ValidationError = validationErrors.ToArray();
            }
            catch (Exception)
            {
                
                //
            }

            return response;
        }

        #endregion ValidationFields

    }
}