﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace PatternRecognition.Dto
{
    [DataContract]
    public class ValidationErrorsType
    {
        /// <summary>
        /// Error Message
        /// </summary>
        [DataMember(Name = "errorMessage")]
        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Field reference
        /// </summary>
        [DataMember(Name = "fieldReference")]
        [JsonProperty("fieldReference")]
        public string FieldReference { get; set; }

    }
}