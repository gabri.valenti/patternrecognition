﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace PatternRecognition.Dto
{
    [DataContract]
    public class GenericErrors
    {
        /// <summary>
        /// Extended info
        /// </summary>
        [DataMember(Name = "extendedInfo")]
        [JsonProperty("extendedInfo")]
        public string ExtendedInfo { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        [DataMember(Name = "message")]
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Validation Errors
        /// </summary>
        [DataMember(Name = "validationErrors")]
        [JsonProperty("validationErrors")]
        public ValidationErrorsType[] ValidationError { get; set; }
    }
}