﻿using NLog;
using System;
using System.Configuration;
using NLog.Config;
using NLog.Targets;
using System.Web.Http;
using System.Net.Http;
using System.Reflection;
using PatternRecognitionLogic;
using PatternRecognition.Utils;
using PatternRecognitionResources.PatternDto;

namespace PatternRecognition.Controllers
{
    public class PatternRecognitionController : ApiController
    {

        private readonly Logger Logger = CreateNLog(ConfigurationManager.AppSettings["FileLogName"], ConfigurationManager.AppSettings["FileLogName"]);

        /*************************************************************/
        //Gestione eccezioni

        /// <summary>
        /// Gestione globale delle eccezione
        /// </summary>
        /// <param name="func">Action da iniettare</param>
        /// <returns>Ritorna un requestDto</returns>
        private HttpResponseMessage ManageException(Func<HttpResponseMessage> func)
        {
            HttpResponseMessage result;

            try
            {
                result = func();
            }
            catch (Exception ex)
            {
                return GenericHelper.CreateResponse(Request, ex);
            }

            return result;
        }


        /*************************************************************/


        /// <summary>
        /// POST /point: Aggiunto un punto nello spazio
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("point")]
        [ModelStateValidationActionFilter]
        public HttpResponseMessage AddPointToSpace([FromBody]PointDto data)
        {
            return ManageException(() => new Logic(Logger).AddPointToSpace(Request, data));
        }

        /// <summary>
        /// GET /space: Recupera i punti dello spazio
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("space")]
        [ModelStateValidationActionFilter]
        public HttpResponseMessage GetSpace()
        {
            return ManageException(() => new Logic(Logger).GetSpace(Request));
        }

        /// <summary>
        /// DELETE /space: Cancella tutti i punti dello spazio
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route("space")]
        [ModelStateValidationActionFilter]
        public HttpResponseMessage DeleteSpace()
        {
            return ManageException(() => new Logic(Logger).DeleteSpace(Request));
        }

        /// <summary>
        /// GET /space: Recupera i punti dello spazio
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("lines/{n}")]
        [ModelStateValidationActionFilter]
        public HttpResponseMessage GetLines([FromUri]int n)
        {
            return ManageException(() => new Logic(Logger).GetLines(Request, n));
        }


        /**********************************************************************/

        /// <summary>
        /// Create Log file
        /// </summary>
        public static Logger CreateNLog(string logName, string objectName)
        {
            // Step 1. Create configuration object 
            var config = new LoggingConfiguration();

            // Step 2. Create targets
            var consoleTarget = new ColoredConsoleTarget("targetConsole")
            {
                Layout = @"${date:format=HH\:mm\:ss} ${level} ${message} ${exception}"
            };
            config.AddTarget(consoleTarget);

            var fileTargetLogHubject = new FileTarget($"targetFileLog{objectName}")
            {
                FileName = "${basedir}\\App_Data\\Log\\${shortdate}_" + $"{logName}.log",
                Layout = "${date}|${level:uppercase=true}||Message: ${message}"
            };
            config.AddTarget(fileTargetLogHubject);

            string name = $"targetFileLog{objectName}Logger";

            // Step 3. Define rules
            config.AddRuleForAllLevels(consoleTarget, name);
            config.AddRuleForAllLevels(fileTargetLogHubject, name);

            // Step 4. Activate the configuration
            LogManager.Configuration = config;

            return LogManager.GetLogger(name);
        }
    }
}