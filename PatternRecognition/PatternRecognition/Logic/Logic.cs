﻿using DAL;
using NLog;
using DAL.Dto;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Collections.Generic;
using PatternRecognitionResources.Math;
using PatternRecognitionResources.GenericDto;
using PatternRecognitionResources.PatternDto;

namespace PatternRecognitionLogic
{
    public class Logic
    {
        private DALInterface DAL { get; }

        private Logger Logger { get; }

        /******************************************************************************************/
        //Costruttore

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="logger"></param>
        public Logic(Logger logger)
        {
            DAL = new DALInterface();
            Logger = logger;
        }

        /******************************************************************************************/
        //Metodi

        /// <summary>
        /// Aggiunge un punto nello spazio
        /// </summary>
        /// <param name="request"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public HttpResponseMessage AddPointToSpace(HttpRequestMessage request, PointDto data)
        {
            Logger.Info("Try to add a point in the space");

            if (data != null) Logger.Info($"Point - x: {data.X}, y: {data.Y}");

            ResultDto<PointResultDto> result = DAL.InsertPoint(data);

            PrintResult(result);

            //Creo il messaggio di risposta
            return request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Recupera tutti i punti dello spazio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public HttpResponseMessage GetSpace(HttpRequestMessage request)
        {
            Logger.Info("Try to get all point in the space");

            ResultDto<List<PointDto>> result = DAL.GetSpace();

            PrintResult(result);

            //Creo il messaggio di risposta
            return request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Cancella tutti i punti dello spazio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public HttpResponseMessage DeleteSpace(HttpRequestMessage request)
        {
            Logger.Info("Try to delete all point in the space");

            ResultDto<bool> result = DAL.DeleteSpace();

            PrintResult(result);

            //Creo il messaggio di risposta
            return request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Recupera tutte le linee passenti per n punti
        /// </summary>
        /// <param name="request"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public HttpResponseMessage GetLines(HttpRequestMessage request, int n)
        {
            Logger.Info($"Try to delete all lines in the space passing for {n} points");

            ResultDto<List<PointDto>> space = DAL.GetSpace();

            List<LineDto> lines = null;

            if (space.Status && space.Data != null && space.Data.Any())
            {
                lines = new List<LineDto>();

                foreach (IEnumerable<PointDto> points in space.Data.Combinations(n))
                {
                    var pointDtos = points as PointDto[] ?? points.ToArray();

                    if (points != null && pointDtos.Any())
                    {
                        LineDto line = new LineDto { Points = pointDtos.ToList() };
                        lines.Add(line);
                    }
                }
            }

            ResultDto<List<LineDto>> result = new ResultDto<List<LineDto>>
            {
                Data = lines,
                Status = true,
                FunctionName = MethodBase.GetCurrentMethod().Name,
                Message = (lines == null ? "No points in the space" : $"Lines number: {lines.Count}")
            };

            PrintResult(result);

            //Creo il messaggio di risposta
            return request.CreateResponse(HttpStatusCode.OK, result);
        }

        /******************************************************************************************/
        //Private

        /// <summary>
        /// Stampa su Log il risultato ottenuto dal DAL
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result"></param>
        private void PrintResult<T>(ResultDto<T> result)
        {
            string message = $"Function: {result.FunctionName}, status {result.Status}, message: {result.Message}";

            if (!result.Status) Logger.Error(message);
            else Logger.Info(message);
        }
    }
}
