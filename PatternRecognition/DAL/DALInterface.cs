﻿using System;
using LiteDB;
using DAL.Dto;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using PatternRecognitionResources.GenericDto;
using PatternRecognitionResources.PatternDto;

namespace DAL
{
    public class DALInterface
    {
        private static readonly string DatabaseName = $"{AppDomain.CurrentDomain.BaseDirectory}\\Database\\PatternRecognitionDatabase.db";

        /****************************************************/

        /// <summary>
        /// Inserisce un nuovo punto nel database
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public ResultDto<PointResultDto> InsertPoint(PointDto point)
        {
            int id = -1;

            PointResultDto result;

            try
            {
                using (var db = new LiteDatabase(DatabaseName))
                {
                    // Get points collection
                    var pointCollections = db.GetCollection<DB_PointDto>("Points");

                    //Get all points
                    var points = pointCollections.FindAll().ToList();

                    //Check if already exist this point
                    if (points != null && points.Any())
                    {
                        var pointInDb = points.Find(x => x.X == point.X && x.Y == point.Y);

                        if (pointInDb != null)
                        {
                            result = new PointResultDto
                            {
                                X = pointInDb.X,
                                Y = pointInDb.Y,
                                Id = pointInDb.Id
                            };

                            id = pointInDb.Id;

                            return new ResultDto<PointResultDto>(result, true, $"Record already exist with id: {id}");
                        }
                    }

                    // Insert new point in points collection (Id will be auto-incremented)
                    id = pointCollections.Insert(new DB_PointDto
                    {
                        X = point.X,
                        Y = point.Y
                    });

                    result = new PointResultDto
                    {
                        X = point.X,
                        Y = point.Y,
                        Id = id
                    };
                }
            }
            catch (Exception ex)
            {
                return new ResultDto<PointResultDto>(ex);
            }

            return new ResultDto<PointResultDto>(result, true, $"Add new record with id: {id}");
        }

        /****************************************************/

        /// <summary>
        /// Recupera tutti i punti
        /// </summary>
        /// <returns></returns>
        public ResultDto<List<PointDto>> GetSpace()
        {
            List<PointDto> pointsResult = null;

            try
            {
                using (var db = new LiteDatabase(DatabaseName))
                {
                    // Get points collection
                    var pointCollections = db.GetCollection<DB_PointDto>("Points");

                    //Get all points
                    var points = pointCollections.FindAll().ToList();

                    if (points != null && points.Any())
                    {
                        pointsResult = new List<PointDto>();

                        foreach (var point in points)
                        {
                            pointsResult.Add(new PointDto { X = point.X, Y = point.Y});
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResultDto<List<PointDto>>(ex);
            }

            return new ResultDto<List<PointDto>>(pointsResult, true, (pointsResult != null ? $"Points number: {pointsResult.Count}" : "No points in the space"));
        }

        /****************************************************/

        /// <summary>
        /// Cancella tutti i punti dallo spazion
        /// </summary>
        /// <returns></returns>
        public ResultDto<bool> DeleteSpace()
        {
            try
            {
                using (var db = new LiteDatabase(DatabaseName))
                {
                    // Get points collection
                    var pointCollections = db.GetCollection<DB_PointDto>("Points");

                    //Get all points
                    var points = pointCollections.FindAll().ToList();

                    if (points != null && points.Any())
                    {
                        foreach (var point in points)
                        {
                            pointCollections.Delete(x => x.Id == point.Id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResultDto<bool>(ex);
            }

            return new ResultDto<bool>(true, true, "Delete all points in the space");
        }

        /****************************************************/

    }
}
