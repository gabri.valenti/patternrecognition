﻿namespace DAL.Dto
{
    public class DB_PointDto
    {
        public int Id { get; set; }

        public double X { get; set; }

        public double Y { get; set; }
    }
}
