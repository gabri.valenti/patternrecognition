Programming Test - Pattern Recognition

Folder description:
1) Documents: contains specification.
2) PatternRecognition: contains source code (ASP. NET Web API).
3) Release: contains release files to publish on IIS.
4) Images: contains images to explain something.
5) Postman: a json file XPatternRecognition.postman_collection.json contains postman api collection. 
5) Node: contains web api server write in node js,


In order to test ASP. NET solution follow the instructions below:
1) Copy the files contained in the release folder on C:\inetpub\wwwroot\{name_you_like}.
2) Right click on the folder C:\inetpub\wwwroot\{name_you_like} and add full control permissions to the IIS user (IIS_IUSRS, see image IIS_USER.PNG in Images folder). 
   This step is important in order to allow iis process to create local database and write log.
3) Open IIS (windows key and write iis), and add new website (see image in ADD_WEB_SITE.PNG in images).
4) Configure web site (see image in WEB_SITE_CONFIGURATION.PNG). It's important to use a port not used by other applications.
5) After configuration will appear new web site under folder siti (see image in WEB_SITE.PNG in images).
6) It's possible to see web api syntax in Images\API_.PNG or load in postman XPatternRecognition.postman_collection.json file.


In order to test node js solution follow the instructions below:
1) Open node command prompt
2) Change directory to node folder
3) using command npm start
4) It's possible to see web api syntax in Images\API_.PNG or load in postman XPatternRecognition.postman_collection.json file (default port setted in code is 30001)

Using postman collection to test web api.

